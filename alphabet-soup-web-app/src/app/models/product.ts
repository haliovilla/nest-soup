export interface CreateProductDTO {
  name: string;
  description: string;
  imageURL: string;
  price: number;
  readonly createdAt?: Date;
}
