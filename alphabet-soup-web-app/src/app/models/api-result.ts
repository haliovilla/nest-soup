import { LetterLocation } from "./LetterLocation";

export interface ApiResult {
  word: string;
  wordExists: boolean;
  direction: string;
  locations: LetterLocation[];
}
