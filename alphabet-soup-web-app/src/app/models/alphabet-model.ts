export interface AlphabetModel {
  AlphabetSoup: string[];
  WordToFind: string;
  SoupSize: number;
}
