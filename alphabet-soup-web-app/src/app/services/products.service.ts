import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { CreateProductDTO } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  apiUrl: string = "localhost:3000/product";

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get("http://localhost:3000/product")
      .pipe(map((res: any) => {
        return res;
      }));
  }

  create() {
    const newProduct: CreateProductDTO = {
      name: "My new product",
      description: "My new product description",
      price: 1234.56,
      imageURL: ""
    };
    const uri = "http://localhost:3000/product/create";
    return this.http.post(uri, newProduct)
      .pipe(map((res: any) => {
        return res;
      }));
  }
}
