import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AlphabetModule } from './alphabet/alphabet.module';

@Module({
  imports: [AlphabetModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
