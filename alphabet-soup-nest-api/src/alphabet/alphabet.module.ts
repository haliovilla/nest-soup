import { Module } from '@nestjs/common';
import { AlphabetController } from './alphabet.controller';
import { AlphabetService } from './alphabet.service';

@Module({
  controllers: [AlphabetController],
  providers: [AlphabetService]
})
export class AlphabetModule {}
