import { ValidationResultDto } from './validation-result.dto';

describe('ValidationResultDto', () => {
  it('should be defined', () => {
    expect(new ValidationResultDto()).toBeDefined();
  });
});
