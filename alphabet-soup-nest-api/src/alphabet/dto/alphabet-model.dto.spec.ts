import { AlphabetModelDto } from './alphabet-model.dto';

describe('AlphabetModelDto', () => {
  it('should be defined', () => {
    expect(new AlphabetModelDto()).toBeDefined();
  });
});
