export class AlphabetModelDto {
    readonly AlphabetSoup: string[];
    readonly WordToFind: string;
    readonly SoupSize: number;
}
