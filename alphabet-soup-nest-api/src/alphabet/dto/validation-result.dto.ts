import { LetterLocation } from "../interfaces/letter-location.interface";

export class ValidationResultDto {
    readonly word: string;
    readonly wordExists: boolean;
    readonly direction: string;
    readonly locations: LetterLocation[];
}
