import { Controller, Get, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { AlphabetService } from './alphabet.service';
import { AlphabetModelDto } from './dto/alphabet-model.dto';
import { ValidationResultDto } from './dto/validation-result.dto';
import { OperationResult } from './interfaces/OperationResult';

@Controller('alphabet')
export class AlphabetController {

    constructor(private service: AlphabetService) { }

    @Post()
    find(@Res() res, @Body() alphabetModel: AlphabetModelDto) {
        const result: ValidationResultDto = this.service.validate(alphabetModel);
        console.log(result.word, result.locations);
        return res.status(HttpStatus.OK).json(result);
    }

}
