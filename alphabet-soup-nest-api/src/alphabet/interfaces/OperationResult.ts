export interface OperationResult {
    Word: string;
    WordExists: boolean;
    Direction: string;
}