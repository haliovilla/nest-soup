
export interface Direction {
    Horizontal: string;
    HorizontalInverted: string;
    Vertical: string;
    VerticalInverted: string;
    Diagonal: string;
    DiagonalInverted: string;
    Special: string;
    SpecialInverted: string;
}

export const Directions: Direction = {
    Horizontal: "Horizontal",
    HorizontalInverted: "Horizontal Invertida",
    Vertical: "Vertical",
    VerticalInverted: "Vertical Invertida",
    Diagonal: "Diagonal",
    DiagonalInverted: "Diagonal Invertida",
    Special: "Escalera",
    SpecialInverted: "Escalera Invertida",
}