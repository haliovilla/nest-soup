export interface LetterLocation {
    Row: number;
    Column: number;
}
