import { Injectable } from '@nestjs/common';
import { AlphabetModelDto } from './dto/alphabet-model.dto';
import { ValidationResultDto } from './dto/validation-result.dto';
import { Directions } from './interfaces/Directions';
import { LetterLocation } from './interfaces/letter-location.interface';

@Injectable()
export class AlphabetService {

    private soup: Array<Array<string>>;
    private invertedSoup: Array<Array<string>>;
    private soupSize: number;
    private randomWord: String;
    private locations: LetterLocation[];

    public validate(alphabetModel: AlphabetModelDto): ValidationResultDto {
        this.createSoupArray(alphabetModel.AlphabetSoup);
        this.createInvertedSoupArray(alphabetModel.AlphabetSoup);
        this.soupSize = alphabetModel.SoupSize;
        return this.findWord(alphabetModel.WordToFind);
    }

    private createEmptySoupArray(size: number) {
        this.soup = [];
        for (var i = 0; i < size; i++) {
            this.soup[i] = [];
            for (var j = 0; j < size; j++) {
                //this.soup[i].push('');
                this.soup[i].push(`${i}-${j}`);
            }
        }
    }

    private reverseString(value: string) {
        return value.split("").reverse().join("");
    }

    private createSoupArray(soupData: string[]) {
        this.soup = [];
        let c = 0;
        soupData.forEach(line => {
            this.soup[c] = [];
            for (var i = 0; i < line.length; i++) {
                this.soup[c].push(line[i]);
            }
            c++;
        });
    }

    private createInvertedSoupArray(soupData: string[]) {
        this.invertedSoup = [];
        let c = 0;
        soupData.forEach(line => {
            let invertedLine: string = this.reverseString(line);
            this.invertedSoup[c] = [];
            for (var i = 0; i < invertedLine.length; i++) {
                this.invertedSoup[c].push(invertedLine[i]);
            }
            c++;
        });
    }

    private findWord(word: string): ValidationResultDto {
        if (this.search(word)) {
            return {
                word: word,
                wordExists: true,
                direction: Directions.Special,
                locations: this.locations
            };
        }

        if (this.search(this.reverseString(word))) {
            return {
                word: word,
                wordExists: true,
                direction: Directions.Special,
                locations: this.locations
            };
        }

        return {
            word: word,
            wordExists: false,
            direction: "",
            locations: []
        };
    }

    private search(word: string): boolean {
        this.randomWord = word;
        return this.find();
    }

    private find(): boolean {
        for (var i = 0; i < this.soupSize; i++) {
            for (var j = 0; j < this.soupSize; j++) {
                if (this.soup[i][j] == this.randomWord[0]) {
                    if (this.findNext(i, j)) {
                        this.getLetterLocations(i, j);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private findNext(row: number, col: number, wordIndex: number = 0): boolean {
        wordIndex++;
        if (wordIndex == this.randomWord.length)
            return true;
        else {
            let right = this.createRight(row, col);
            let downRight = this.createDownRight(row, col);
            let down = this.createDown(row, col);
            let downLeft = this.createDownLeft(row, col);
            let left = this.createLeft(row, col);
            let upLeft = this.createUpLeft(row, col);
            let up = this.createUp(row, col);
            let upRight = this.createUpRight(row, col);

            if (this.compareLetter(this.randomWord[wordIndex], right)) {
                return this.findNext(right.Row, right.Column, wordIndex);
            }
            if (this.compareLetter(this.randomWord[wordIndex], downRight)) {
                return this.findNext(downRight.Row, downRight.Column, wordIndex);
            }
            if (this.compareLetter(this.randomWord[wordIndex], down)) {
                return this.findNext(down.Row, down.Column, wordIndex);
            }
            if (this.compareLetter(this.randomWord[wordIndex], downLeft)) {
                return this.findNext(downLeft.Row, downLeft.Column, wordIndex);
            }
            if (this.compareLetter(this.randomWord[wordIndex], left)) {
                return this.findNext(left.Row, left.Column, wordIndex);
            }
            if (this.compareLetter(this.randomWord[wordIndex], upLeft)) {
                return this.findNext(upLeft.Row, upLeft.Column, wordIndex);
            }
            if (this.compareLetter(this.randomWord[wordIndex], up)) {
                return this.findNext(up.Row, up.Column, wordIndex);
            }
            if (this.compareLetter(this.randomWord[wordIndex], upRight)) {
                return this.findNext(upRight.Row, upRight.Column, wordIndex);
            }
        }
        return false;
    }

    private createUp(row: number, col: number): LetterLocation {
        if (row > 0)
            row--;
        return {
            Row: row,
            Column: col
        };
    }

    private createDown(row: number, col: number): LetterLocation {
        if (row < this.soupSize - 1)
            row++;
        return {
            Row: row,
            Column: col
        };
    }

    private createLeft(row: number, col: number): LetterLocation {
        if (col > 0)
            col--;
        return {
            Row: row,
            Column: col
        };
    }

    private createRight(row: number, col: number): LetterLocation {
        if (col < this.soupSize - 1)
            col++;
        return {
            Row: row,
            Column: col
        };
    }

    private createUpRight(row: number, col: number): LetterLocation {
        if (row > 0)
            row--;
        if (col < this.soupSize - 1)
            col++;
        return {
            Row: row,
            Column: col
        };
    }

    private createUpLeft(row: number, col: number): LetterLocation {
        if (row > 0)
            row--;
        if (col > 0)
            col--;
        return {
            Row: row,
            Column: col
        };
    }

    private createDownRight(row: number, col: number): LetterLocation {
        if (row < this.soupSize - 1)
            row++;
        if (col < this.soupSize - 1)
            col++;
        return {
            Row: row,
            Column: col
        };
    }

    private createDownLeft(row: number, col: number): LetterLocation {
        if (row < this.soupSize - 1)
            row++;
        if (col > 0)
            col--;
        return {
            Row: row,
            Column: col
        };
    }

    private compareLetter(letter: string, location: LetterLocation): boolean {
        return this.soup[location.Row][location.Column] == letter;
    }

    private getLetterLocations(row: number, col: number) {
        this.locations = [];
        this.locations.push({
            Row: row,
            Column: col
        });
        this.getNextLetterLocation(row, col);
    }

    private getNextLetterLocation(row: number, col: number, wordIndex: number = 0) {
        wordIndex++;
        if (wordIndex == this.randomWord.length)
            return;
        else {
            let right = this.createRight(row, col);
            let downRight = this.createDownRight(row, col);
            let down = this.createDown(row, col);
            let downLeft = this.createDownLeft(row, col);
            let left = this.createLeft(row, col);
            let upLeft = this.createUpLeft(row, col);
            let up = this.createUp(row, col);
            let upRight = this.createUpRight(row, col);

            if (this.compareLetter(this.randomWord[wordIndex], right)) {
                this.locations.push(right);
                this.getNextLetterLocation(right.Row, right.Column, wordIndex);
            }
            else if (this.compareLetter(this.randomWord[wordIndex], downRight)) {
                this.locations.push(downRight);
                this.getNextLetterLocation(downRight.Row, downRight.Column, wordIndex);
            }
            else if (this.compareLetter(this.randomWord[wordIndex], down)) {
                this.locations.push(down);
                this.getNextLetterLocation(down.Row, down.Column, wordIndex);
            }
            else if (this.compareLetter(this.randomWord[wordIndex], downLeft)) {
                this.locations.push(downLeft);
                this.getNextLetterLocation(downLeft.Row, downLeft.Column, wordIndex);
            }
            else if (this.compareLetter(this.randomWord[wordIndex], left)) {
                this.locations.push(left);
                this.getNextLetterLocation(left.Row, left.Column, wordIndex);
            }
            else if (this.compareLetter(this.randomWord[wordIndex], upLeft)) {
                this.locations.push(upLeft);
                this.getNextLetterLocation(upLeft.Row, upLeft.Column, wordIndex);
            }
            else if (this.compareLetter(this.randomWord[wordIndex], up)) {
                this.locations.push(up);
                this.getNextLetterLocation(up.Row, up.Column, wordIndex);
            }
            else if (this.compareLetter(this.randomWord[wordIndex], upRight)) {
                this.locations.push(upRight);
                this.getNextLetterLocation(upRight.Row, upRight.Column, wordIndex);
            }
        }
    }

}